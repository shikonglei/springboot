package com.veromca.springboot.mapper;

import java.util.List;

import com.veromca.springboot.model.User;

/**
 * 
 * @author LiuSongqing
 *
 */
public interface UserMapper {

	public Long save(User user);

	public User getById(Long id);

	public List<User> findList(User user);

	public Long deleteById(Long id);

	public Long delete(User user);

	public Long update(User user);

}
