package com.veromca.springboot.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.veromca.springboot.model.User;
import com.veromca.springboot.service.UserService;

/**
 * 
 * @author LiuSongqing
 *
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {

	private Logger logger = Logger.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	@ResponseBody
	public User getById(@RequestParam("id") Long id) {
		User user = userService.getById(id);
		if (user != null) {
			System.out.println("getById " + user.toString());
			logger.info("getById " + user.toString());
		}
		return user;
	}

	@RequestMapping("/saveUser")
	@ResponseBody
	public Long saveUser(@RequestBody User user) {
		Long count = userService.save(user);
		if (user != null) {
			System.out.println("saveUser success " + user.toString() + " success_sum = " + count);
			logger.info("saveUser success " + user.toString() + " success_sum = " + count);
		}
		return count;
	}

	@RequestMapping("/findUserList")
	@ResponseBody
	public List<User> findUserList(@RequestBody User user) {
		List<User> list = new ArrayList<User>();
		list = userService.findList(user);
		if (list != null && list.size() > 0) {
			System.out.println("findUserList success " + user.toString() + " size = " + list.size());
			logger.info("findUserList success " + user.toString() + " size = " + list.size());
		}
		return list;
	}

	@RequestMapping("/updateUser")
	@ResponseBody
	public Long updateUser(@RequestBody User user) {
		Long count = userService.update(user);
		if (user != null) {
			System.out.println("updateUser success " + user.toString() + " success_sum = " + count);
			logger.info("updateUser success " + user.toString() + " success_sum = " + count);
		}
		return count;
	}

	@RequestMapping("/deleteById")
	@ResponseBody
	public Long deleteById(@RequestParam("id") Long id) {
		Long count = userService.deleteById(id);
		if (count != null) {
			System.out.println("deleteById success id =" + id + " success_sum = " + count);
			logger.info("deleteById success id = " + id + " success_sum = " + count);
		}
		return count;
	}

	@RequestMapping("/delete")
	@ResponseBody
	public Long delete(@RequestBody User user) {
		Long count = userService.delete(user);
		if (count != null) {
			System.out.println("deleteUser success user =" + user.toString() + " success_sum = " + count);
			logger.info("deleteUser success user = " + user.toString() + " success_sum = " + count);
		}
		return count;
	}

	/**
	 * 跳转至jsp
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/toUserInfoJsp", method = RequestMethod.GET)
	public String toUserInfoJsp(HttpServletRequest request, HttpServletResponse response, @RequestParam("id") Long id,
			Model model) {
		User user = userService.getById(id);
		if (user != null) {
			System.out.println("getById " + user.toString());
			logger.info("getById " + user.toString());
			model.addAttribute("userName", user.getName());
			model.addAttribute("user", user);
		}
		model.addAttribute("remark", "springboot 让开发web更简单");
		return "userInfo";
	}
}
