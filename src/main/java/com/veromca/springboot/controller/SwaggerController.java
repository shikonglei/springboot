package com.veromca.springboot.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.veromca.springboot.model.User;
import com.veromca.springboot.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 
 * @author LiuSongqing
 *
 */
@RestController
@RequestMapping(value = "/swagger")
public class SwaggerController {
	private Logger logger = Logger.getLogger(SwaggerController.class);
	@Autowired
	private UserService userService;

	/*
	 * 访问地址 ：http://localhost:8080/swagger-ui.html
	 */

	/**
	 * 获取所有用户
	 * 
	 * @return
	 */
	@ApiOperation(value = "Get all users", notes = "获取所有用户信息")
	@RequestMapping(value = "/getUsers", method = RequestMethod.GET)
	public List<User> getUsers() {
		List<User> list = new ArrayList<User>();
		User user = new User();
		list = userService.findList(user);
		if (list != null && list.size() > 0) {
			System.out.println("findList success size:" + list.size());
			logger.info("findList success size:" + list.size());
		}
		return list;
	}

	@ApiOperation(value = "Get user with id", notes = "requires the id of user")
	@RequestMapping(value = "/{name}", method = RequestMethod.GET)
	public User getUserById(@ApiParam(required = true, name = "name", value = "姓名") @PathVariable String name) {
		User user = new User();
		user.setName("hello world + " + name);
		return user;
	}
}
