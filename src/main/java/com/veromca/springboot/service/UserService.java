package com.veromca.springboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.veromca.springboot.mapper.UserMapper;
import com.veromca.springboot.model.User;

/**
 * 
 * @author LiuSongqing
 *
 */

@Service
public class UserService {

	@Autowired
	private UserMapper userMapper;

	public User getById(Long id) {
		User user = userMapper.getById(id);
		return user;
	}

	public Long save(User user) {
		Long count = userMapper.save(user);
		return count;
	}

	public List<User> findList(User user) {
		List<User> list = userMapper.findList(user);
		return list;
	}

	public Long deleteById(Long id) {
		Long count = userMapper.deleteById(id);
		return count;
	}

	public Long delete(User user) {
		Long count = userMapper.delete(user);
		return count;
	}

	public Long update(User user) {
		Long count = userMapper.update(user);
		return count;
	}

}
