git地址：
https://git.oschina.net/veromca/springboot.git

Java bean User
	private String id;
    private String name;
    private Integer age;
    private String password;
	
数据库脚本见：/springboot/src/main/resources/db_user.sql	

http接口测试地址：

http://localhost:8080/user/getById?id=1

localhost:8080/user/saveUser?id=2&name=刘德华&age=40&password=123456

localhost:8080/user/saveUser?id=3&name=delete&age=40&password=123456

localhost:8080/user/saveUser?id=4&name=lisi&age=40&password=123456

localhost:8080/user/findUserList

localhost:8080/user/updateUser?id=2&name=程序猿&age=30&password=123456

localhost:8080/user/deleteById?id=3

localhost:8080/user/delete?name=lisi


